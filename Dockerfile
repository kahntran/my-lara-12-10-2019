# Multi-Stage Approach

# Stage 1

#
# PHP Dependencies
#
FROM composer:1.7 as vendor

COPY database/ database/

COPY composer.json composer.json
COPY composer.lock composer.lock

RUN composer global require hirak/prestissimo && composer install \
    --ignore-platform-reqs \
    --no-interaction \
    --no-plugins \
    --no-scripts \
    --prefer-dist

# Stage 2

#
# Frontend
#
#FROM node:8.11 as frontend
#
#RUN mkdir -p /app/public
#
#COPY package.json webpack.mix.js yarn.lock /app/
#COPY resources/assets/ /app/resources/assets/
#
#WORKDIR /app
#
#RUN yarn install && yarn production

# Stage 3

#
# Application
#
FROM php:7.2-alpine

EXPOSE 9000
WORKDIR /var/www/html

COPY . /var/www/html
COPY --from=vendor /app/vendor/ /var/www/html/vendor/
#COPY --from=frontend /app/public/js/ /var/www/html/public/js/
#COPY --from=frontend /app/public/css/ /var/www/html/public/css/
#COPY --from=frontend /app/mix-manifest.json /var/www/html/mix-manifest.json

RUN cp .env.example .env && php artisan key:generate

CMD php artisan serve --host 0.0.0.0 --port 9000
